/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 03
 * Purpose: Main
 * Notes:
 * *************************************************************************/
#include "main.h"

//Stack accumulator is initialized.
int Stack::stackCounter = 0;

int main(){
    std::cout << "Hello, World!" << std::endl;
    return 0;
}