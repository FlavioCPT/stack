
/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 04
 * Purpose: Stack definition
 * Notes:
 * *************************************************************************/
#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <string>
using namespace std;

struct StackLevel{
    int id;
    string data;
    StackLevel *next;
};

class Stack{
private:
    //Declared accumulator for levels.
    static int stackCounter;

    //Bool to help with increasing or decreasing
    //the accumulator.
    bool incrOrDecr;

    //Pointers able to point to a stack level.
    struct StackLevel *top;
    struct StackLevel *tempPtr;

public:
    //CONSTRUCTOR.
    Stack();

    //MEMBER FUNCTIONS.
    //Adds one to the current value of the
    //stack counter.
    void setStackLevel(bool);

    //Retrieves private value of the stack
    //counter.
    int getStackLevel();

    //Adds a new topmost value to the stack.
    void push(string);

    //Deletes the topmost value of the stack.
    string pop();

    //Ecoes values inside structure to the screen.
    void peek();
};
#endif //STACK_H
