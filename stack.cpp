/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 03
 * Purpose: Stack implementation
 * Notes:
 * *************************************************************************/
#include "stack.h"

//- - - - - - - - - - - - - - - - - -
//   ***STACK COUNTER MUTATOR***
//- - - - - - - - - - - - - - - - - -
void Stack::setStackLevel(bool incrOrDecr){
    if (incrOrDecr == true){stackCounter++;}
    else if((incrOrDecr == false) && (stackCounter == 0)){}
    else if((incrOrDecr == false) && (stackCounter != 0)){stackCounter--;};
}

//- - - - - - - - - - - - - - - - - -
//   ***STACK COUNTER ACCESSOR***
//- - - - - - - - - - - - - - - - - -
int Stack::getStackLevel(){
    return stackCounter;
}

//- - - - - - - - - - - - - - - -
//   ***ADD LAYER TO STACK***
//- - - - - - - - - - - - - - - -
void Stack::push(string dataToAdd){
    //Stack counter goes up by one.
    incrOrDecr = true;
    setStackLevel(incrOrDecr);

    //This is the pointer used to add new levels to the stack.
    struct StackLevel *levelCreation = nullptr;

    //When no stack levels exist, the first one is made.
    if(top == nullptr){
        levelCreation = top;

        levelCreation = new StackLevel;
        levelCreation->id = getStackLevel();
        levelCreation->data = dataToAdd;
        levelCreation->next = nullptr;

        top = levelCreation;

    //Else, when stack levels already exist, the new one is
    //added to the top.
    }else{
        tempPtr = top;

        levelCreation = new StackLevel;
        levelCreation->id = getStackLevel();
        levelCreation->data = dataToAdd;
        levelCreation->next = tempPtr;

        top = levelCreation;
    };
}

//- - - - - - - - - - - - - - - - - -
//   ***REMOVE LAYER FROM STACK***
//- - - - - - - - - - - - - - - - - -
string Stack::pop(){
    //This variable will hold the string that will be deleted.
    string delData;

    //This is the pointer used to erase the top level of the stack.
    struct StackLevel *levelDestruction = nullptr;

    //Here the condition of a stack underflow is caught.
    if(top == nullptr){
        delData = "Stack Underflow.";

    //The topmost level is deleted.
    }else{
        tempPtr = top;
        top = top->next;

        delData = tempPtr->data;
        delete tempPtr;
        tempPtr = nullptr;

        incrOrDecr = false;
        setStackLevel(incrOrDecr);
    };

    return delData;
}

//- - - - - - - - - - - - - - - -
//   ***ECO TOP LAYER'S INFO**
//- - - - - - - - - - - - - - - -
void Stack::peek(){
    cout << "Topmost level of the stack" << endl;
    cout << "- - - - - - - - - - - - - -" << endl;
    cout << "Level ID: " << top->id << endl;
    cout << "Level data: " << top->data << endl << endl;
}

//- - - - - - - - - - - -
//   ***CONSTRUCTOR***
//- - - - - - - - - - - -
Stack::Stack(){
    top = nullptr;
    tempPtr = nullptr;
}